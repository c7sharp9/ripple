package ripple::history;

use Modern::Perl;
use Moo;
use List::Util qw(max min);
use Types::Standard qw(ArrayRef Int Maybe Ref Str);

has file => (is => 'rw', isa => Str);
has lines => (is => 'rw', isa => ArrayRef[Str], default => sub {[]});
has max_history => (is => 'rw', isa => Maybe[Int]);
has rpl => (is => 'rw', isa => Maybe[Ref], weak_ref => 1);

# proxies
sub verbose { shift->rpl->verbose }
sub rl { shift->rpl->rl }

sub trim {
	my($self,@hist) = @_;
	@hist = @{$self->lines} unless @hist;
	my $maxh = $self->max_history // 0;
	shift(@hist) while ($maxh && 0+@hist >= $maxh);
	return @hist;
}
sub append {
	my($self,$input) = @_;
	my $fn = $self->file;
	my @hist = $self->trim();
	my $max = $self->max_history // 0;
	warn("# saving history => $fn\n") if $self->verbose;
	warn("# new input: |$input|, previous history ".
	     scalar(@hist).": ".join(", ",@hist)."\n")
		if $self->verbose > 1;
	push(@hist,$input);
	my $f = IO::File->new("${fn}.tmp", ">:utf8")
		or die "rl canot save ${fn}.tmp: $!";
	$f->print("$_\n") foreach @hist;
	$f->close();
	die "unlink ${fn}.bak: $!"
		if -f "${fn}.bak" && !unlink("${fn}.bak");
	die "rename ${fn} ${fn}.bak: $!"
		unless (!-f $fn) || rename($fn,"${fn}.bak");
	die "rename ${fn}.tmp ${fn}: $!"
		unless rename("${fn}.tmp",$fn);
	return $self;
}
sub overwrite {
	my($self,$rl) = @_;
	my $fn = $self->file;
	$rl //= $self->rl;
	die "no rl!" unless $rl;
	my $f = IO::File->new($fn, ">:utf8")
		or die "rl cannot write $fn: $!";
	$f->print("$_\n") for ($self->trim());
	$f->close();
	return $self;
}
sub load {
	my($self,$rl) = @_;
	my $history_file = $self->file;
	$rl //= $self->rl;
	warn("# loading history: $history_file\n") if $self->verbose;
	if (!(-f $history_file)) {
		warn("# history empty - skipped\n") if $self->verbose;
		return $self;
	}
	my $f = IO::File->new($history_file,"<:utf8")
		or die "rl history file $history_file: $!\n";
	my @lines;
	while (defined(my $line = $f->getline())) {
		$line =~ s/\s+$//;
		next unless $line;
		push(@lines,$line);
	}
	$f->close();
	@lines = $self->trim(@lines);
	$self->lines([@lines]);
	$rl->clear_history();
	$rl->add_history($_) foreach @lines;
	return $self;
}
sub list {
	my($self,$n,$rl) = @_;
	$rl //= $self->rl;
	my @hist = @{$self->lines};
	my $k = $#hist;
	my @seq = ($n > 0)? (max(($k-$n)+1,0) .. $k):
		(($n < 0)? (0 .. min(abs($n)-1,$k)): (0 .. $k));
	warn("# list n=$n k=$k seq=@seq\n") if $self->verbose;
	warn("$_: ".$hist[$_]."\n") foreach (@seq);
	return $self;
}
sub clear {
	my($self,$n,$rl) = @_;
	$rl //= $self->rl;
	my @hist = @{$self->lines};
	my $k = $#hist;
	$n //= 0;
	my @seq = ($n > 0)? (max(($k-$n)+1,0) .. $k):
		(($n < 0)? (0 .. min(abs($n)-1,$k)): (0 .. $k));
	@hist = splice(@hist,@seq);
	$self->lines([@hist]);
	warn("# clear n=$n k=$k seq=@seq new=".scalar(@hist)."\n")
		if $self->verbose;
	$rl->clear_history();
	$rl->add_history($_) for (@hist);
	return $self->overwrite();
}
sub grep {
	my($self,$pat,$rl) = @_;
	$rl //= $self->rl;
	my @hist = @{$self->lines};
	warn("# grep /$pat/\n") if $self->verbose;
	warn("$_: ".$hist[$_]."\n")
		foreach (grep { $hist[$_] =~ /$pat/ } (0 .. $#hist));
	return $self;
}

1;
