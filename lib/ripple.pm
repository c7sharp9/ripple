package ripple;

use Modern::Perl;
use Moo;
use Cwd qw(getcwd);
use Getopt::Long qw(GetOptionsFromArray);
use Term::ReadLine;
use Types::Standard qw(CodeRef Enum InstanceOf Int Maybe Str);
use ripple::history;

our $VERSION = '1.0.0';
our %STYLES = (
	bold => 'so,se,se,se',
	underline => 'us,ue,ue,ue',
	plain => '',
);

has verbose => (is => 'rw', isa => Int, default => 0);
has prog => (is => 'rw', isa => Str, default => 'ripple');
has chdir => (is => 'rw', isa => Maybe[Str]);
has style => (is => 'rw', isa => Enum[keys(%STYLES)], default => 'plain');
has rl => (is => 'rw', isa => Maybe[InstanceOf[qw(Term::ReadLine)]]);
has prompt => (is => 'rw', isa => Maybe[CodeRef]);
has history => (is => 'rw', isa => Maybe[InstanceOf[qw(ripple::history)]]);
has preput => (is => 'rw', isa => Maybe[Str]);
has default_prompt => (is => 'rw', isa => Maybe[Str]);
has _xit => (is => 'rw', isa => Maybe[Int]);

sub init {
	my($self) = @_;
	$SIG{INT}  = sub { warn(" INTERRUPT!\n"); $self->_xit(202); };
	$SIG{ALRM} = sub { warn(" ALARM!\n"); $self->_xit(200); };
	$SIG{QUIT} = sub { warn(" QUIT!\n"); $self->_xit(201); };
#	$SIG{INFO} = sub { exit(202); } if exists $SIG{INFO};
	$self;
}

sub parse_cla {
	my($self,@argv) = @_;
	my($help,$no_history,$history_file,$verbose,$style,
	   $max_history,@completes,$chdir,$preput);
	Getopt::Long::Configure("bundling");
	GetOptionsFromArray(
		\@argv,
		'h|help' => \$help,
		'H|no-history' => \$no_history,
		'F|history=s' => \$history_file,
		'X|max-history=i' => \$max_history,
		'S|style=s' => \$style,
		'C|complete=s' => \@completes,
		'D|chdir=s' => \$chdir,
		'v|verbose+' => \$verbose,
		'P|preput=s' => \$preput,
	) or pod2usage();
	$verbose //= 0;
	pod2usage(-verbose => 1+$verbose) if $help;
	$style //= 'plain';
	pod2usage(-msg => "invalid style: $style")
		unless exists $STYLES{$style};
	die "no such dir: $chdir\n"
		if $chdir && ! -d $chdir;
	$self->chdir($chdir);
	my $prog = shift(@argv) || 'rl';
	$self->prog($prog);
	my $prompt = join(" ",@argv) || "> ";
	$prompt = "[${prog}] ${prompt}" if $prog ne 'rl';
	$self->default_prompt($prompt);
	# set process name for ps(1)
	my $procname = "${prog}";
	$procname .= " ${prompt}" if $verbose;
	$::0 = $procname;
	$self->rl(Term::ReadLine->new($prog));
	# style
	my $ornaments = $STYLES{$style};
	$self->rl->ornaments($ornaments);
	# history
	$history_file //= join('/',$ENV{HOME},".${prog}.history")
		unless $no_history;
	warn("# $prog history file: $history_file\n")
		if $history_file && $verbose;
	my $history =
		ripple::history->new(
			rpl => $self,
			file => $history_file,
			max_history=>$max_history,
		)->load() if !$no_history && $history_file;
	# completion: try @completes first and fall back to filenames
	$self->history($history);
	if (@completes) {
		my $attribs = $self->rl->Attribs;
		$attribs->{attempted_completion_function} = sub {
			my($text,$line,$start,$end) = @_;
			if (substr($line,0,$start) =~ /^\s*$/) {
				return $self->rl->completion_matches(
					$text,
					$attribs->{list_completion_function})
			} else {
				return ();
			}
		};
		@completes = map { split(/,/,$_) } @completes;
		$attribs->{completion_word} = \@completes;
	}
	return $self;
}

sub prompt_user {
	my($self) = @_;
	my $chdir = $self->chdir;
	my($xit,$prompt);
	my $history = $self->history;
	my $preput = $self->preput;
	my $here = getcwd();
 INPUT:	chdir($chdir) if $chdir;
	$xit = undef;
	$prompt = $self->prompt->() if $self->prompt;
	$prompt //= $self->default_prompt;
	my $input = $self->rl->readline($prompt,$preput);
	$xit = $self->_xit;
	$self->preput(undef);
	chdir($here) if $self->chdir;
	if (!defined($input)) {
		$xit //= 1;
	} elsif (!$xit) {
		chomp($input);
		goto INPUT unless $input;
		$history->append($input) if $history;
	}
	$input =~ s/(^\s+|\s+$)//gs;
	return $input unless wantarray;
	return($input,$xit);
}

1;

__END__

=pod

=head1 NAME

rl - read a line from tty with editing and history

=head1 SYNOPSIS

rl [-vH] [-F histfile] [-X maxhist] [-S style] [-C word[,...]] [-D dir] progname [prompt...]

Options:

    --help       -?        print this manual
    --verbose    -v        spew messages about loading/saving history
    --no-history -H        do not load/save history this invocation
    --history    -F file   load/save history from/to this file
    --max-history -X int   maximum history size in lines (def: none)
    --style      -S style  one of: bold, underline or plain (def: plain)
    --complete   -C word   add word to completion list (more than once)
    --chdir       -D dir   chdir to dir when reading input (for fn complete)

=head1 DESCRIPTION
	

Read a line from the terminal w/command line editing, completion and
history support.  By default the command history will go in
C<~/.progname.history>, e.g. if you invoke us like so:

    rl prog 'INBOX*> '

history will be restored/saved from/to C<~/.prog.history>.  Whatever
line we finally read is spit out on stdout.

If no C<--complete> options are given filename completion will be done
by default; you can control which directory to complete relative to
with the C<--chdir> option.  If any C<--complete> options are given,
the total set of words (after splitting on comma) will be treated as
commands that should be the initial word of any line.  Words beyond
the first will use filename completion, which will also be used as the
fall-back if no completion word matches the initial input.

Command-line Options:

=over 4

=item * C<--help>, C<-?>

Print some part of this manual.

=item * C<--verbose>, C<-v>

Combined with C<--help> prints the whole manual.
Otherwise, enables debug output on stderr.

=item * C<-no-history>, C<-H>

Do not load/save history.

=item *  C<--history=file>, C<-F file> 

Load/save history from/to this file.

=item * C<--max-history=int>, <-X int>

Maximum history size in lines (def: none)

=item * C<--style=style>,  C<-S style>

Style to apply to the prompt.  One of: bold, underline or plain (def:
plain).

=item * C<--complete=word[,word...]>, C<-C word[,word..]>

Add word(s) to completion list.  Can be given more than once and each
option's value is also treated as a comma-separated list, so C<-C one
-C two> and C<-C one,two> are the same.

=item * C<--chdir=dir>, C<-D dir>

Chdir to dir when reading input, so that filename completion has the
deswired effect.

=back

We handle input lines that start with a period ourselves.
They look like C<.command [args...]>.

There is currently one internal command, C<.history>.

=over 4

=item * .history [number]

With no arguments lists the latest 10 entries in the history

If the number is negative, that many entries from the beginning
of the history are listed.  If the number is postive, that many
entries from recent history, and if zero the whole history
is listed to stderr.

=item * .history /pattern.../

Search the history for the given regexp and print matching entries to
stderr.

=item * .history clear [number]

With no arguments clears all of history.  Otherwise the
interpretation of number is the same as for listing
history.

=back


=cut
