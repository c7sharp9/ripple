#!/usr/bin/env perl

use Modern::Perl;
use Test::More;

# programmatically get the list of modules and require_ok them all
# that way we don't have to constantly edit this test

open(MAN,"MANIFEST") or die "MANIFEST: $!";
our @LIST =(map { $_ =~ s,^lib/,,; $_ =~ s,/,::,g; $_ =~ s/\.pm$//; $_; }
	    grep {/^lib\/.*\.pm$/} <MAN>);
close(MAN);

#warn(sprintf("# %s=%s\n",$_,$ENV{$_})) foreach sort keys %ENV;

require_ok($_) foreach (@LIST);

done_testing(scalar(@LIST));
